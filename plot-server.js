const express = require('express');
var bodyParser = require('body-parser');
var shell = require('shelljs');
const { spawn } = require('child_process');

const app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.post('/action', (req, res) => {
  const body = req.body;

  if (body) {
    if (body.action === 'checkStatus') {
      res.sendStatus(200);
    }

    if (body.action === 'runScript') {
      const data = shell.exec(body.script);
      res.json({ out: data.stdout, err: data.stderr });
    }

    if (body.action === 'runDetach') {
      shell.exec(body.script, { async: true });
      res.end();
    }
  }
});

app.listen(7000, (e) => {
  console.log(e);
  console.log('app start');
});
